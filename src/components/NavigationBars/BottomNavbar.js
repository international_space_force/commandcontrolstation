import React from 'react';
import styled from "styled-components";
import BatteryLife from './Headers/BatteryLife.js';
import Temperature from './Headers/Temperature.js';
import CompanyLogo from '../../assets/ISFLogoGray.png';


/* This defines the actual bar going down the screen */
const StyledBottomNav = styled.div`
  position: fixed;     /* Fixed Sidebar (stay in place on scroll and position relative to viewport) */
  height: 80px;
  width: 100%;     /* Set the width of the sidebar */
  z-index: 1;      /* Stay on top of everything */
  background-color: #222; /* Black */
  overflow-x: hidden;     /* Disable horizontal scroll */
  bottom: 0px;
  background-image: linear-gradient(black, grey);
`;

const StyledCompanyLogo = styled.img`
    z-index: 1;
    position: fixed;
    height: 70px;
    width: 250px;
    bottom: 0;
`;

class BottomNav extends React.Component {
    constructor(){
        super();
        this.state = {
            temperature: '79',
        }
    }

    componentDidMount(){
        const ws = new WebSocket("ws://localhost:4208");
        ws.addEventListener("open", () => {
            console.log("We are connected!");
            ws.send("This is from the Bottom Navbar.");
        });

        ws.addEventListener("message", ({ data }) => {
            //console.log(data);
            var string = data.toString();
            if (string.includes("Temperature")){
                var temperatureValue = string.substr(13, 13);
                this.setState({
                    temperature : temperatureValue
                })
            }
            else if (string.includes("Connected")){
                
            }
        });
    }

    render () {
        return (
            <div>
                <StyledBottomNav></StyledBottomNav>
                <BatteryLife></BatteryLife>
                <Temperature desiredScale='f' givenScale='f' value={this.state.temperature} />
                <StyledCompanyLogo src={CompanyLogo} alt="ISFLogo" />
            </div>
        );
    }
}

//                <img src={AerialView} alt="AerialView" />


export default class BottomNavbar extends React.Component {
    render() {
        return (
            <div className="BottomNav">
                <BottomNav></BottomNav>
            </div>
        );
    }
}