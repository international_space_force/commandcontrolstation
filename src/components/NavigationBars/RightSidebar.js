import React from 'react';
import styled from "styled-components";
import OperatorOptions from './Headers/OperatorOptions.js';

/* This defines the actual bar going down the screen */
const StyledRightSideNav = styled.div`
  position: fixed;     /* Fixed Sidebar (stay in place on scroll and position relative to viewport) */
  height: 100%;
  width: 300px;     /* Set the width of the sidebar */
  z-index: 1;      /* Stay on top of everything */
  top: 3.4em;      /* Stay at the top */
  background-color: #A9A9A9; /* Grey */
  overflow-x: hidden;     /* Disable horizontal scroll */
  padding-top: 10px;
  right: 0px;
  background-image: radial-gradient(white, grey);
`;

class RightNav extends React.Component {
    render (){
        return (
            <div>
                <StyledRightSideNav></StyledRightSideNav>
                <OperatorOptions></OperatorOptions>
            </div>
        );
    }
}

export default class RightSidebar extends React.Component {
    render() {
        return (
            <div className="SideNav">
                <RightNav></RightNav>
            </div>
        );
    }
}