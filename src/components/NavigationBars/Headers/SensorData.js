import React from 'react';
import styled from "styled-components";
import redCircle from '../../../assets/redCircle.png';
import greyCircle from '../../../assets/greyCircle.png';

const SensorDataText = styled.h2`
    color: black;
    font-weight: bold;
    background-color: #D3D3D3;
    z-index: 1;
    position: fixed;
    font-family: sans-serif;
    width: 300px;
    font-size: medium;
    text-align: center;
    margin-top: 10px;
`;

const UltrasonicDetectionText = styled.h3`
    color: black;
    z-index: 1;
    position: fixed;
    font-family: sans-serif;
    width: 100px;
    font-size: small;
    text-align: left;
    margin-top: 40px;
    font-weight: bold;
    margin-left: 50px;
`;

const IEDDetectionText = styled.h3`
    color: black;
    z-index: 1;
    position: fixed;
    font-family: sans-serif;
    width: 100px;
    font-size: small;
    text-align: left;
    margin-top: 50px;
    font-weight: bold;
    margin-left: 175px;
`;

class Detection extends React.Component {
    render (){
        return (
            <div>
                <UltrasonicDetectionText>Ultrasonic Detection</UltrasonicDetectionText>
                <IEDDetectionText>IED Detection</IEDDetectionText>
            </div>
        );
    }
}

export default class SensorData extends React.Component {
    constructor(props) {
        super(props);
      }

      udDetection() {
        switch(this.props.udDetected) {
          case 'Y':
            return <img className="UDRedCircle" src={redCircle} alt="Red" />;
          default:
              return <img className="UDGreyCircle" src={greyCircle} alt="Grey" />;
        }
      }

      iedDetection() {
          switch (this.props.iedDetected){
            case 'Y':
                return <img className="IEDRedCircle" src={redCircle} alt="Red" />;
            default:
                return <img className="IEDGreyCircle" src={greyCircle} alt="Grey" />;
          }
      }
    render (){
        return (
            <div>
                <SensorDataText className="Headings">Sensor Data</SensorDataText>
                <Detection></Detection>
                <span>{this.udDetection()}
                {this.iedDetection()}</span>
            </div>
        );
    }
}

/* 
When sensor data is integrated, add if-statement and change circles based on
detection status:

Ultrasonic Detection Grey Circle:
<img className="UDGreyCircle" src={greyCircle} alt="Grey" />;

IED Detection Red Circle:
<img className="IEDRedCircle" src={redCircle} alt="Red" />;

Have to test the css position because did not test when writing this code
*/