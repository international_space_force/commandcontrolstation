import React from 'react';

export default class Temperature extends React.Component {
    constructor(props) {
      super(props);
    }
    fToC(f) {
      return (f - 32) * (5 / 9);
    }
    cToF(c) {
      return (c * (9 / 5)) + 32;
    }
    value() {
      switch(this.props.desiredScale) {
        case 'c':
          return this.props.givenScale === 'c' ? this.props.value : this.fToC(this.props.value);
        case 'f':
          return this.props.givenScale === 'f' ? this.props.value : this.cToF(this.props.value);
        default:
          throw 'BORKED!';
      }
    }
    render() {
      return (
            <span className="temperature">{this.value()}{'\u00b0'}F</span>
      );
    }
  }