import React from 'react';
import styled from "styled-components";
import CameraPlaceholder from '../../../assets/cameraPlaceholder.png';

const CameraViewText = styled.h2`
    color: black;
    font-weight: bold;
    background-color: #D3D3D3;
    z-index: 1;
    position: fixed;
    font-family: sans-serif;
    width: 300px;
    font-size: medium;
    text-align: center;
    margin-top: 10px;
    right: 0px;
`;

/* const videoConstraints = {
    width: 100,
    height: 100,
    facingMode: "user"
}; */

export default class CameraView extends React.Component {
    render (){
        return (
            <div>
                <CameraViewText className="Headings">Camera View</CameraViewText>
                <img src={CameraPlaceholder} alt='Camera' style={{
                    zIndex:'1',
                    position:'fixed',
                    right:'0px',
                    marginRight:'70px',
                    marginTop:'50px'
                    }}/>              
            </div>
        );
    }
}

/* 
Webcam Component:
*Add below CameraViewText in render method

<Webcam 
    audio={false}
    height={100}
    width={100}
    videoConstraints={videoConstraints}
    style={{
        zIndex:'1',
        position:'fixed',
        right:'0',
        marginRight:'90px',
        marginTop:'60px',
        border:'solid'
    }}
/>
*/