import React from 'react';
import ReactSpeedometer from "react-d3-speedometer";

const textColor = "#AAA";

export default class Speedometer extends React.Component {
    render () {
        return (
            <div>
                <ReactSpeedometer
                needleHeightRatio={0.7}
                maxSegmentLabels={6}
                segments={5555}
                value={30}
                textColor={textColor}
                maxValue={150}
                    />
            </div>
        );
    }
}

//className="speedometer"