import React from 'react';
import styled from "styled-components";
import axios from 'axios';
import API from '../../../../src/API.js';

const OperatorOptionsText = styled.h2`
    color: black;
    font-weight: bold;
    background-color: #D3D3D3;
    z-index: 1;
    position: fixed;
    font-family: sans-serif;
    width: 300px;
    font-size: medium;
    text-align: center;
    margin-top: 25px;
    right: 0px;
`;

const ManualOverrideText = styled.h3`
    color: black;
    z-index: 1;
    position: fixed;
    font-family: sans-serif;
    width: 300px;
    font-size: small;
    text-align: center;
    margin-top: 60px;
    padding-left: 50px;
    right: 0px;
    font-weight: bold;
`;

const KillSwitchText = styled.h3`
    color: black;
    z-index: 1;
    position: fixed;
    font-family: sans-serif;
    width: 300px;
    font-size: small;
    text-align: center;
    margin-top: 125px;
    padding-left: 50px;
    right: 0px;
    font-weight: bold;
`;

const MissionExecutiveText = styled.h3`
    color: black;
    z-index: 1;
    position: fixed;
    font-family: sans-serif;
    width: 300px;
    font-size: small;
    text-align: center;
    margin-top: 200px;
    padding-left: 50px;
    right: 0px;
    font-weight: bold;
`;

const NotificationsText = styled.h1`
    color: black;
    z-index: 1;
    position: fixed;
    font-family: sans-serif;
    width: 300px;
    font-size: large;
    text-align: center;
    margin-top: 400px;
    padding-left: 50px;
    right: 0px;
    font-weight: bold;
`;


const ws = new WebSocket("ws://localhost:4208");

class Buttons extends React.Component {
    constructor(){
        super();
        this.state = {
            manualModeOn: 'N',
            killSwitchOn: 'N',
            missionExecStart: 'N',
            destinationReached: 'N'
        }
    }

    componentDidMount(){
        ws.addEventListener("open", () => {
            console.log("We are connected!");
            ws.send("This is from the Operator Options Component.");
            ws.send("Can you see me client?");
        });

        ws.addEventListener("message", ({ data }) => {
            var string = data.toString();
            if (string.includes("Manual Mode")){
                var manualMode = string.substr(13, 13);
                //console.log(manualMode);
                this.setState({
                    manualModeOn : manualMode
                });
                //console.log(this.state.manualModeOn);
                if (this.state.manualModeOn === 'Y'){
                    var manualButton = document.getElementById('manualOverrideSwitch');
                    manualButton.checked = true;
                }
                else if (this.state.manualModeOn === 'N'){
                    var manualButton = document.getElementById('manualOverrideSwitch');
                    manualButton.checked = false;
                }
            }
            else if (string.includes("Kill Switch Status")){
                var killSwitch = string.substr(20, 20);
                //console.log(killSwitch);
                this.setState({
                    killSwitchOn : killSwitch
                });
                //console.log(this.state.killSwitchOn);
                if (this.state.killSwitchOn === 'Y'){
                    var killButton = document.getElementById('killSwitch');
                    killButton.checked = true;
                }
                else if (this.state.killSwitchOn === 'N'){
                    var killButton = document.getElementById('killSwitch');
                    killButton.checked = false;
                }
            }
            else if (string.includes("Mission Exec")){
                var missionExec = string.substr(14, 14);
                //console.log(killSwitch);
                this.setState({
                    missionExecStart : missionExec
                });
                //console.log(this.state.killSwitchOn);
                if (this.state.missionExecStart === 'Y'){
                    console.log("Mission Executive Started - came from message");
                }
                else if (this.state.killSwitchOn === 'N'){
                    console.log("Mission Executive Stopped - came from message");
                }
            }
            else if (string.includes("Destination")){
                var destReached = string.substr(13, 13);
                //console.log(killSwitch);
                this.setState({
                    destinationReached : destReached
                });
                //console.log(this.state.killSwitchOn);
                if (this.state.destinationReached === 'Y'){
                    console.log("Destination Reached");
                }
            }
        });
    }

    handleManualModeChange(e){
        console.log('value of checkbox : ', e.target.checked);
        if (e.target.checked){
            this.setState({
                manualModeOn : 'Y'
            })

            ws.send("Manual Mode button turned on");
        } 
        else {
            ws.send("Manual Mode button turned off");
        }
    }

    handleKillSwitchChange(e){
        console.log('Kill switch button clicked : ', e.target.checked);
        if (e.target.checked){
            this.setState({
                killSwitchOn : 'Y'
            })

            ws.send("Kill Switch button turned on");
        } 
        else {
            ws.send("Kill Switch button turned off");
        }
    }

    handleStartSubmit(e){
        console.log("Mission Executive Button Submitted");
        ws.send("Mission Executive Button Submitted");
    }

    handleStopSubmit(e){
        console.log("Mission Executive Button Stopped");
        ws.send("Mission Executive Button Stopped");
    }

    displayNotification(){
        if (this.state.destinationReached === 'Y'){
            return "Destination Reached"
        }
        else{
            return '';
        }
    }

    render (){
        return (
            <div>
                <ManualOverrideText>Manual Override</ManualOverrideText>
                <div className="custom-control custom-switch"
                style={{
                    zIndex:'1',
                    position:'fixed',
                    right:'0',
                    marginTop:'80px',
                    marginRight:'100px'
                }}>
                    <input type="checkbox" className="custom-control-input" id="manualOverrideSwitch" onChange={this.handleManualModeChange.bind(this)}></input>
                    <label className="custom-control-label" htmlFor="manualOverrideSwitch"></label>
                </div>
                <KillSwitchText>Kill Switch</KillSwitchText>
                <div className="custom-control custom-switch"
                style={{
                    zIndex:'1',
                    position:'fixed',
                    right:'0',
                    marginTop:'145px',
                    marginRight:'100px'
                }}>
                    <input type="checkbox" className="custom-control-input" id="killSwitch" onChange={this.handleKillSwitchChange.bind(this)}></input>
                    <label className="custom-control-label" htmlFor="killSwitch"></label>
                </div>
                <MissionExecutiveText>Mission Executive</MissionExecutiveText>
                <button type="submit" className="btn btn-success" data-toggle="button" aria-pressed="false" id="start" onClick={this.handleStartSubmit.bind(this)}
                style={{zIndex:'1',
                        position:'fixed',
                        marginTop: '230px',
                        marginRight:'150px',
                        right: '0',
                        backgroundColor:'green',
                        borderColor: 'black'}}>
                    START
                </button>
                <button type="submit" className="btn btn-success" data-toggle="button" aria-pressed="false" id="stop" onClick={this.handleStopSubmit.bind(this)}
                style={{zIndex:'1',
                        position:'fixed',
                        marginTop: '230px',
                        marginRight:'60px',
                        right: '0',
                        backgroundColor:'red',
                        borderColor: 'black'}}>
                    STOP
                </button>
                <NotificationsText>Notifications</NotificationsText>
                <h3
                style={{zIndex:'1',
                        position:'fixed',
                        marginTop: '430px',
                        marginRight:'20px',
                        right: '0',
                        color:'red'}} className="dest">
                {this.displayNotification()}</h3>
            </div>
        );
    }
}

export default class OperatorOptions extends React.Component {
    render (){
        return (
            <div>
                <OperatorOptionsText className="Headings">Operator Options</OperatorOptionsText>
                <Buttons></Buttons>
            </div>
        );
    }
}

{/* <div className="custom-control custom-switch"
                style={{
                    zIndex:'1',
                    position:'fixed',
                    right:'0',
                    marginTop:'205px',
                    marginRight:'100px'
                }}>
                    <input type="checkbox" className="custom-control-input" id="missionExec" onChange={this.handleManualModeChange.bind(this)}></input>
                    <label className="custom-control-label" htmlFor="missionExec"></label>
                </div> */}