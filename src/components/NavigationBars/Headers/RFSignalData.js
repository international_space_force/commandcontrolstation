import React from 'react';
import styled from "styled-components";

const RFSignalText = styled.h2`
    color: black;
    font-weight: bold;
    background-color: #D3D3D3;
    z-index: 1;
    position: fixed;
    font-family: sans-serif;
    width: 300px;
    font-size: medium;
    text-align: center;
    margin-top: 150px;
`;

export default class RFSignalData extends React.Component {
    render (){
        return (
            <RFSignalText className="Headings">RF Signal Data</RFSignalText>
        );
    }
}