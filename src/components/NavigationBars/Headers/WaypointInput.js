import React from 'react';
import styled from "styled-components";
import redMarker from '../../../assets/redMarker.png';

const WaypointInputText = styled.h2`
    color: black;
    font-weight: bold;
    background-color: #D3D3D3;
    z-index: 1;
    position: fixed;
    font-family: sans-serif;
    width: 300px;
    font-size: medium;
    text-align: center;
    margin-top: 250px;
`;

const Latitude = styled.h3`
    color: black;
    font-size: small;
    margin-top: 50px;
    font-weight: bold;
    margin-left: 120px;
`;

const Longitude = styled.h3`
    color: black;
    font-size: small;
    margin-top: 0px;
    font-weight: bold;
    margin-left: 120px;
`;

export default class WaypointInput extends React.Component {
    constructor(){
        super();
        this.state = {
            Latitude: '',
            Longitude: '',
            marker: ''
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleMarkerChange = this.handleMarkerChange.bind(this);
    }

    handleChange = (event) => {
        this.setState({[event.target.name]: [event.target.value]});
    }

    handleMarkerChange = (event) => {
        this.setState({redMarker});
    }

    handleClick = () => {
        console.log("Button clicked!");
    }
    
    handleSubmit = (event) => {
        alert('Latitude and Longitude Submitted - Latitude: ' + this.state.Latitude + ' Longitude: ' + this.state.Longitude + '.');
        event.preventDefault();
        var lat = this.state.Latitude;
        var long = this.state.Longitude;
        this.props.setInputPoints({Latitude: lat, Longitude: long});
    }

    handleMarkerPlaceholder = () => {
        
    }

    /* 
    Place below span above div

    <img src={this.state.marker} alt="redMarker" style={{
                        zIndex:'1',
                        position:'fixed',
                        height:'100px',
                        width:'95px',
                        marginLeft:'900px',
                        marginTop:'30px'
                        }}
                    /> */

    render (){
        return (
            <div>
                <WaypointInputText className="Headings">Waypoint Input</WaypointInputText>
                <form className="inputForm" onSubmit={this.handleMarkerChange}>
                    <span>
                        <div style={{paddingBottom:'10px'}}>
                            <Latitude>Latitude: </Latitude>
                            <input className="inputTextStyle" type="text" name="Latitude" onChange={this.handleChange} style={{marginLeft:'105px'}}/>
                        </div>
                        <div>
                            <Longitude>Longitude: </Longitude>
                            <input className="inputTextStyle" type="text" name="Longitude" onChange={this.handleChange} style={{marginLeft:'105px'}}/>
                        </div>
                    </span>
                <button type="submit" className="btn btn-success" data-toggle="button" aria-pressed="false" onClick={this.handleSubmit.bind(this)}
                style={{zIndex:'1',
                        position:'fixed',
                        marginTop:'20px',
                        marginLeft:'125px',
                        backgroundColor:'green',
                        borderColor: 'black'}}>
                    GO
                </button>
                </form>
            </div>
        );
    }
}
