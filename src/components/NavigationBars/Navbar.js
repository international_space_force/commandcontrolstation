import React from 'react'
import { Link } from 'react-router-dom';

function Navbar() {
    return (
      <nav className="navbar navbar-expand-lg navbar-light bg-dark">
        <Link className="navbar-brand text-white text-uppercase" to="/">BOLT System</Link>
        <button className="navbar-toggler" type="button" data-toggle="collapse" 
        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" 
        aria-expanded="false" aria-label="Toggle navigation">
          <span>
            <i className="fas fa-bars" style={{ color: '#fff' }}></i>
          </span>
        </button>

        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav m-auto">
            <li className="nav-item active">
              <Link className="nav-link text-white text-uppercase ml-5" to="/">Home&nbsp;<i className="fas fa-home"></i><span className="sr-only">(current)</span></Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link text-white text-uppercase ml-5" to="/routes">Routes</Link>
            </li>
          </ul>
        </div>
      </nav>  
    );
}

export default Navbar;

/* 
Drop Down list:
       <li className="nav-item dropdown">
        <Link className="nav-link dropdown-toggle text-white text-uppercase ml-5" to="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Operator Options
        </Link>
        <div className="dropdown-menu text-uppercase" aria-labelledby="navbarDropdown">
          <Link className="dropdown-item" to="/routes">View Current Route</Link>
          <Link className="dropdown-item" to="/points-of-interest">View Points of Interest</Link>
          <div className="dropdown-divider"></div>
          <Link className="dropdown-item" to="/manual-override">Manual Override</Link>
        </div>
      </li>
        
Search:
    <form className="form-inline my-2 my-lg-0">
      <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search"/>
      <button className="btn btn-outline-primary my-2 my-sm-0" type="submit">Search</button>
    </form>

        */