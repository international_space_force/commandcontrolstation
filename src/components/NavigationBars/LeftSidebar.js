import React from 'react';
import styled from "styled-components";
import SensorData from './Headers/SensorData.js';
import WaypointInput from './Headers/WaypointInput.js';

/* This defines the actual bar going down the screen */
const StyledLeftSideNav = styled.div`
  position: fixed;     /* Fixed Sidebar (stay in place on scroll and position relative to viewport) */
  height: 100%;
  width: 300px;     /* Set the width of the sidebar */
  z-index: 1;      /* Stay on top of everything */
  top: 3.4em;      /* Stay at the top */
  background-color: #A9A9A9; /* Grey */
  overflow-x: hidden;     /* Disable horizontal scroll */
  padding-top: 10px;
  background-image: radial-gradient(white, grey);
`;

class LeftNav extends React.Component {
    constructor(){
        super();
        this.state = {
            inputs: [],
            Latitude: '',
            Longitude: '',
            udDetected: 'N',
            iedDetected: 'N'
        }
    }

    componentDidMount(){
        const ws = new WebSocket("ws://localhost:4208");
        ws.addEventListener("open", () => {
            console.log("We are connected!");
            ws.send("This is from the Left Sidebar.");
        });

        ws.addEventListener("message", ({ data }) => {
            //console.log(data);
            var string = data.toString();
            if (string.includes("IED Detection")){
                var iedDetection = string.substr(15, 15);
                this.setState({
                    iedDetected : iedDetection
                })
            }
            if (string.includes("Ultrasonic Detection")){
                var ultrasonicDetection = string.substr(22, 22);
                this.setState({
                    udDetected : ultrasonicDetection
                })
            }
        });
    }

 

    sidebarFunction = (waypointInput)=>{
        this.props.setLeftSidebar({Latitude : waypointInput.Latitude, Longitude : waypointInput.Longitude});
    }

    render () {
        return (
            <div>
                <StyledLeftSideNav></StyledLeftSideNav>
                <SensorData udDetected={this.state.udDetected} iedDetected={this.state.iedDetected}></SensorData>
                <WaypointInput setInputPoints={this.sidebarFunction.bind(this)}></WaypointInput>
            </div>
        );
    }
}

export default class LeftSidebar extends React.Component {
    constructor(){
        super();
        this.state = {
            Latitude: '',
            Longitude: ''
        }
    }

    sidebarFunction = (waypointInput)=>{
        this.props.setLeftSidebar({Latitude : waypointInput.Latitude, Longitude : waypointInput.Longitude});
    }

    render() {
        return (
            <div className="SideNav">
                <LeftNav setLeftSidebar={this.sidebarFunction.bind(this)}></LeftNav>
            </div>
        );
    }
}

 

