import React, { Component } from 'react';
import L, { map } from 'leaflet';
import {Map, TileLayer, Marker, Popup, Polyline} from 'react-leaflet';
import redMarker from '../../assets/redMarker.png';
import greenMarker from '../../assets/greenMarker.png';
//import blueMarker from '../../assets/blueMarker.png';
import fob from '../../assets/fob.png';
import WaypointInput from '../NavigationBars/Headers/WaypointInput.js';

class PointsOfInterest extends Component {
  state = {
    vehicleStart: {
      lat: 42.237039,
      lng: -76.052836,
    },
    oneSoldier: {
      lat: 42.236922,
      lng: -76.053025,
    },
    twoSoldiers: {
      lat: 42.236867,
      lng: -76.052597,
    },
    fourSoldiers: {
      lat: 42.236717,
      lng: -76.052786,
    },
    fob: {
      lat: 42.237139,
      lng: -76.052847,
    },
    waypointInput: {
      lat: '',
      lng: '',
    },
    zoom: 40
  }

/*     waypointInput: {
      lat: 42.236938,
      lng: -76.052900,
    }, 
      waypointInput = L.icon({
    iconUrl: redMarker,
    iconSize:     [25, 25], // size of the icon
  });
  */


  vehicleStart = L.icon({
    iconUrl: redMarker,
    iconSize:     [25, 25], // size of the icon
  });

  oneSoldier = L.icon({
    iconUrl: greenMarker,
    iconSize: [25, 25],
  });

  twoSoldiers = L.icon({
    iconUrl: greenMarker,
    iconSize: [25, 25],
  });

  fourSoldiers = L.icon({
    iconUrl: greenMarker,
    iconSize: [25, 25],
  });

  fob = L.icon({
    iconUrl: fob,
    iconSize:     [25, 25], // size of the icon
  });

  boundary1 = new L.LatLng(42.237147,-76.05285);
  boundary2 = new L.LatLng(42.236961, -76.052358);
  boundary3 = new L.LatLng(42.236689, -76.052781);
  boundary4 = new L.LatLng(42.236881, -76.053275);
  polylinePoints = [this.boundary1, this.boundary2, this.boundary3, this.boundary4];
  polylinePoints2 = [this.boundary1, this.boundary4];

  /* this.state.waypointInput.map((name, number) => {
      this.state.waypointInput.lat = new L.LatLng(
        name.Latitude, name.Longitude)
  }); */

  polyline = L.polyline(this.polylinePoints);
  polyline2 = L.polyline(this.polylinePoints2);

  render(){
    const positionOneSoldier = [this.state.oneSoldier.lat, this.state.oneSoldier.lng];
    const positionVehicleStart = [this.state.vehicleStart.lat, this.state.vehicleStart.lng];
    const positionTwoSoldiers = [this.state.twoSoldiers.lat, this.state.twoSoldiers.lng];
    const positionFourSoldiers = [this.state.fourSoldiers.lat, this.state.fourSoldiers.lng];
    const positionFob = [this.state.fob.lat, this.state.fob.lng];
    //console.log(this.props.inputFromParent);
    //const waypointInputPosition = [this.state.waypointInput.lat, this.state.waypointInput.lng];
    return (
      <Map style={{position:'fixed', marginLeft:'200px'}} className="map" center={positionFob} zoom={this.state.zoom}>
        <TileLayer
          attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        <Marker position={positionOneSoldier} icon={this.oneSoldier}>
          <Popup>
           1 Soldier
          </Popup>
        </Marker>
        <Marker position={positionTwoSoldiers} icon={this.twoSoldiers}>
          <Popup>
          2 Soldiers
          </Popup>
        </Marker>
        <Marker position={positionFourSoldiers} icon={this.fourSoldiers}>
          <Popup>
          4 Soldiers
          </Popup>
        </Marker>
        <Marker position={positionFob} icon={this.fob}>
          <Popup>
          Forward Operating Base
          </Popup>
        </Marker>
        <Marker position={positionVehicleStart} icon={this.vehicleStart}>
          <Popup>
          Vehicle Start
          </Popup>
        </Marker>
        <Polyline positions={this.polylinePoints}></Polyline>
        <Polyline positions={this.polylinePoints2}></Polyline>
      </Map>
    );
  }
}

export default PointsOfInterest;

{/*         <Marker position={waypointInputPosition} icon={this.waypointInput}>
          <Popup>
          New Waypoint
          </Popup>
        </Marker> */}