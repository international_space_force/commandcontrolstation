import React, { Component } from 'react';
//import { InfoConsumer } from '../Context';
import LeftSidebar from '../NavigationBars/LeftSidebar.js';
import RightSidebar from '../NavigationBars/RightSidebar.js';
import BottomNavbar from '../NavigationBars/BottomNavbar.js';
import PointsOfInterest from './PointsOfInterest.js';

class Home extends Component {
    constructor(){
        super();
        this.state = {
            inputs: [],
            Latitude: '',
            Longitude: ''
        }
    }

    parentFunction=(waypointInput)=>{
        console.log("Latitude : " + waypointInput.Latitude);
        console.log("Longitude : " + waypointInput.Longitude);

        var lat = waypointInput.Latitude;
        var long = waypointInput.Longitude;

        this.setLatLong(lat, long);
    }

    setLatLong(latitude, longitude){
        this.setState({
            Latitude: latitude, Longitude: longitude
        }, () => {
            console.log("State: " + this.state.Latitude + " State: " + this.state.Longitude);
            this.state.inputs.push(this.state.Latitude, this.state.Longitude);
            console.log(this.state.inputs);
        });
    }
    
    render() {
        return (
           <div className="App">
                <PointsOfInterest inputFromParent={this.state.inputs}></PointsOfInterest>
                <LeftSidebar style={{zIndex:'1', position:'fixed'}} setLeftSidebar={this.parentFunction.bind(this)}/>
                <RightSidebar/>
                <BottomNavbar/>
            </div>
        )
    }
}

export default Home;

/*  <InfoConsumer>
                {data => {
                    return <h2 className="titleText">{data}</h2>
                }}
            </InfoConsumer> */

// style={{zIndex:'-1', position:'fixed'}}