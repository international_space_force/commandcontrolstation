import React, { Component } from 'react'
import PointsOfInterest from './PointsOfInterest.js';
import RoutesSidebar from './RoutesSidebar.js';

class Routes extends Component {
    render() {
        return (
            <div>
                <RoutesSidebar/>
                <PointsOfInterest/>
            </div>
        )
    }
}

export default Routes;