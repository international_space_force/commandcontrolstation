import React from 'react';
import styled from "styled-components";
import ListGroup from "react-bootstrap/ListGroup";
//import RoutesSidebarInit from "../NavigationBars/RoutesSidebarInit.js";

/* This defines the actual bar going down the screen */
const StyledLeftSideNav = styled.div`
  position: fixed;     /* Fixed Sidebar (stay in place on scroll and position relative to viewport) */
  height: 100%;
  width: 300px;     /* Set the width of the sidebar */
  z-index: 1;      /* Stay on top of everything */
  top: 3.4em;      /* Stay at the top */
  background-color: #A9A9A9; /* Grey */
  overflow-x: hidden;     /* Disable horizontal scroll */
  padding-top: 10px;
  background-image: radial-gradient(white, grey);
`;

/* const RoutesList = [
    {
        name: 'To the FOB'
    },
    {
        name: 'To Soldier 1'
    },
    {
        name: 'To Soldier 2'
    }
]; */

class LeftNav extends React.Component {
    render () {
        return (
            <div>
                <StyledLeftSideNav></StyledLeftSideNav>
                <span style={{
                    "zIndex":"1", 
                    "position":"fixed", 
                    "width":"300px",
                    }}>
                    <ListGroup defaultActiveKey="#link1">
                        <ListGroup.Item action href="#link1">
                            To the FOB
                        </ListGroup.Item>
                        <ListGroup.Item action href="#link2">
                            To Soldier 1
                        </ListGroup.Item>
                        <ListGroup.Item action href="#link3">
                            To Soldier 2
                        </ListGroup.Item>
                    </ListGroup>
                </span>
            </div>
        );
    }
}

export default class RoutesSidebar extends React.Component {
    render() {
        return (
            <div className="SideNav">
                <LeftNav></LeftNav>
            </div>
        );
    }
}