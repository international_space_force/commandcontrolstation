import React, { Component } from 'react'

const InfoContext = React.createContext();

// Provider
// Consumer

class InfoProvider extends Component {
    render() {
        return (
            <InfoContext.Provider value="Welcome to the BOLT Command and Control Station">
                {this.props.children}
            </InfoContext.Provider>
        )
    }
}

const InfoConsumer = InfoContext.Consumer;

export { InfoProvider, InfoConsumer };