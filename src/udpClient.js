var mav = require('./v2_js.js')
var udp = require('dgram');

var buffer = require('buffer');

// creating a client socket
var client = udp.createSocket('udp4');
const WebSocket = require('ws');

const ws = new WebSocket("ws://localhost:4208");
ws.addEventListener("open", () => {
	console.log("We are connected!");
	ws.send("This is from the udpClient.");
});

ws.addEventListener("message", ({ data }) => {
	var string = data.toString();
	var binaryData = '';
	var valid = false;

	if (string.includes("Manual Mode On")){
		console.log("This is the message coming in on udpClient.js (Manual Mode On) : " + string);
		mavMsg = new mav.mavlink.messages.command_int(8, 10, 0, mav.mavlink.MAV_CMD_DO_SET_MODE, 0, 0, 
			mav.mavlink.MAV_MODE_MANUAL_ARMED, 0, 0, 0, 0, 0, 0);
		binaryData = new Buffer(mavMsg.pack(mavMsg));
		valid = true;
	}
	else if (string.includes("Manual Mode Off")){
		console.log("This is the message coming in on udpClient.js (Manual Mode Off) : " + string);
		mavMsg = new mav.mavlink.messages.command_int(8, 10, 0, mav.mavlink.MAV_CMD_DO_SET_MODE, 0, 0, 
			mav.mavlink.MAV_MODE_MANUAL_DISARMED, 0, 0, 0, 0, 0, 0);
		binaryData = new Buffer(mavMsg.pack(mavMsg));
		valid = true;	
	}
	else if (string.includes("Kill Switch On")){
		console.log("This is the message coming in on udpClient.js (Kill Switch On) : " + string);
		mavMsg = new mav.mavlink.messages.command_int(8, 10, 0, mav.mavlink.MAV_CMD_DO_FLIGHTTERMINATION, 0, 0, 
			1, 0, 0, 0, 0, 0, 0);
		binaryData = new Buffer(mavMsg.pack(mavMsg));
		valid = true;		
	}
	else if (string.includes("Kill Switch Off")){
		console.log("This is the message coming in on udpClient.js (Kill Switch Off) : " + string);
		mavMsg = new mav.mavlink.messages.command_int(8, 10, 0, mav.mavlink.MAV_CMD_DO_FLIGHTTERMINATION, 0, 0, 
			0, 0, 0, 0, 0, 0, 0);
		binaryData = new Buffer(mavMsg.pack(mavMsg));
		valid = true;		
	}
	else if (string.includes("Start Mission Exec")){
		console.log("This is the message coming in on udpClient.js (Start Mission Exec) : " + string);
		mavMsg = new mav.mavlink.messages.command_int(8, 10, 0, mav.mavlink.MAV_CMD_DO_PAUSE_CONTINUE, 0, 0, 
			1, 0, 0, 0, 0, 0, 0);
		binaryData = new Buffer(mavMsg.pack(mavMsg));
		valid = true;	
	}
	else if (string.includes("Stop Mission Exec")){
		console.log("This is the message coming in on udpClient.js (Stop Mission Exec) : " + string);
		mavMsg = new mav.mavlink.messages.command_int(8, 10, 0, mav.mavlink.MAV_CMD_DO_PAUSE_CONTINUE, 0, 0, 
			0, 0, 0, 0, 0, 0, 0);
		binaryData = new Buffer(mavMsg.pack(mavMsg));
		valid = true;	
	}
	
	if(valid) {
		client.send(binaryData, 4210, 'localhost', function(error) {
			if(error) {
				client.close();
			} else {
				console.log('Message Sent');
			}
		});
	}
});



//buffer msg
var data = Buffer.from('siddheshrane');

client.on('message', function(msg,info) {
	console.log('Data received from server : ' + msg.toString());
	console.log('Received %d bytes from %s:%d\n',msg.length, info.address, info.port);
});

//sending multiple msg
//sending msg
/* client.send(data, 4210, 'localhost', function(error) {
		if(error) {
			client.close();
		} else {
			console.log('Message Sent');
		}
	});

var i;
for (i = 0; i < 10; i++) { 
	client.send([data1,data2],4208,'localhost',function(error){
	if(error){
		client.close();
	} else {
		console.log('Data sent !!!');
	}
	
	});
} */

