var mav = require('./v2_js.js')
var udp = require('dgram');

 
//var testVariable = 'Y';
const WebSocket = require('ws');
const wss = new WebSocket.Server({ port: 4208 })

// creating a udp server
var server = udp.createSocket('udp4');

// emits when any error occurs
server.on('error',function(error){
  console.log('Error: ' + error);
  server.close();

});
 

var messageType  = 'None';
var iedDetection = '';
var manualModeOn = '';
var ultrasonicDetection = '';
var temperature = '';
var killSwitchOn = '';
var missionExecStart = '';
var destReached = '';
// emits on new datagram msg

server.on('message',function(msg,info){
  var mavlinkMsg = mav.MAVLinkProcessor.prototype.decode(msg);
  console.log('Data ID from client : ' + mavlinkMsg.header.msgId);
  switch (mavlinkMsg.header.msgId){
    case 28:
      console.log('Data -> Temperature ' + mavlinkMsg.temperature);
      messageType = 'Temperature';
      temperature = mavlinkMsg.temperature;
    break;
    case 42:
      console.log('Data -> IED Detection ' + mavlinkMsg.seq);
      messageType = 'IED Detection';
      if (mavlinkMsg.seq === 1){
        iedDetection = 'Y';
      }
      else {
        iedDetection = 'N';
      }
    break;
    case 46:
      console.log('Data -> Destination Reached ' + mavlinkMsg.seq);
      messageType = 'Destination';
      if (mavlinkMsg.seq === 1){
        destReached = 'Y';
      }
      else {
        destReached = 'N';
      }
    break;
    case 75:
      switch (mavlinkMsg.command){
        case 176:
          console.log('Data -> Manual Mode Status ' + mavlinkMsg.param1);
          messageType = 'Manual Mode';
          if (mavlinkMsg.param1 === 192){
            manualModeOn = 'Y';
          }
          else {
            manualModeOn = 'N';
          }
        break;
        case 185:
          console.log('Data -> Kill Switch Status ' + mavlinkMsg.param1);
          messageType = 'Kill Switch';
          if (mavlinkMsg.param1 == 1){
            killSwitchOn = 'Y';
          }
          else {
            killSwitchOn = 'N';
          }
        break;
        case 193:
          console.log('Data -> Mission Exec Mission Status ' + mavlinkMsg.param1);
          messageType = 'Mission Exec';
          if (mavlinkMsg.param1 == 1){
            missionExecStart = 'Y';
          }
          else {
            missionExecStart = 'N';
          }
        default:
          console.log("CMD number not found.");
        break;
        }
    break;
    case 247:
        console.log('Data -> Ultrasonic Detection ' + mavlinkMsg.id);
        messageType = 'Ultrasonic Detection';
        ultrasonicDetection = 'Y';
    break;
    default:
      console.log('Message ID not recognized.');
    break;
  }

  console.log('Data received from client : ' + msg.toString());
  console.log('Received %d bytes from %s:%d\n',msg.length, info.address, info.port);

  //sending msg

  server.send(msg,info.port,'localhost',function(error){
    if(error){
      client.close();
    } else {
      console.log('Data sent');
    }
  });
});

wss.on("connection", ws => {
    console.log("New client connected");

    // Test Switch Statement
/*     switch (46){
        case 28:
          var temp = '68';
          console.log('Data -> Temperature ' + temp);
          messageType = 'Temperature';
          temperature = temp;
        break;
        case 42:
          var seq = 1;
          console.log('Data -> IED Detection ' + seq);
          messageType = 'IED Detection';
          if (seq === 1){
            iedDetection = 'Y';
          }
          else {
            iedDetection = 'N';
          }
        break;
        case 46:
          var seq = 1;
          console.log('Data -> Destination Reached ' + seq);
          messageType = 'Destination';
          if (seq === 1){
            destReached = 'Y';
          }
          else {
            destReached = 'N';
          }
        break;
        case 75:
          var param1 = 192;
          console.log('Data -> Manual Mode Status ' + param1);
          messageType = 'Manual Mode';
          if (param1 === 192){
            manualModeOn = 'Y';
          }
          else {
            manualModeOn = 'N';
          }
        break;
        case 185:
          var param1 = 1;
          console.log('Data -> Kill Switch Status ' + param1);
          messageType = 'Kill Switch';
          if (param1 == 1){
            killSwitchOn = 'Y';
          }
          else {
            killSwitchOn = 'N';
          }
        break;
        default:
          console.log('Message ID not recognized.');
        break;
      } */

    if (messageType == "IED Detection"){
      setInterval(function ping() {ws.send("IED Detection: " + iedDetection)}, 1000);
    }
    if (messageType == "Manual Mode") {
      setInterval(function ping() {ws.send("Manual Mode: " + manualModeOn)}, 1000)
    }
    if (messageType == "Kill Switch") {
      setInterval(function ping() {ws.send("Kill Switch Status: " + killSwitchOn)}, 1000)
    }
    if (messageType == "Mission Exec") {
      setInterval(function ping() {ws.send("Mission Exec: " + missionExecStart)}, 1000)
    }
    if(messageType == "Ultrasonic Detection"){
      setInterval(function ping() {ws.send("Ultrasonic Detection: " + ultrasonicDetection)}, 1000)
    }
    if (messageType == "Temperature"){
      setInterval(function ping() {ws.send("Temperature: " + temperature)}, 1000)
    }
    if (messageType == "Destination"){
      setInterval(function ping() {ws.send("Destination: " + destReached)}, 1000)
    }

    ws.addEventListener("message", ({ data }) => {
      console.log(data);
      var string = data.toString();
      if (string.includes("Manual Mode button turned on")){
        wss.clients.forEach(function each(client) {
          if (client.readyState === WebSocket.OPEN){
            client.send("Manual Mode On");
          }
        })        
      }
      else if (string.includes("Manual Mode button turned off")){
        wss.clients.forEach(function each(client) {
          if (client.readyState === WebSocket.OPEN){
            client.send("Manual Mode Off");
          }
        }) 
      }
      else if (string.includes("Kill Switch button turned on")){
        wss.clients.forEach(function each(client) {
          if (client.readyState === WebSocket.OPEN){
            client.send("Kill Switch On");
          }
        }) 
      }
      else if (string.includes("Kill Switch button turned off")){
        wss.clients.forEach(function each(client) {
          if (client.readyState === WebSocket.OPEN){
            client.send("Kill Switch Off");
          }
        }) 
      }
      else if (string.includes("Mission Executive Button Submitted")){
        wss.clients.forEach(function each(client) {
          if (client.readyState === WebSocket.OPEN){
            client.send("Start Mission Exec");
          }
        }) 
      }
      else if (string.includes("Mission Executive Button Stopped")){
        wss.clients.forEach(function each(client) {
          if (client.readyState === WebSocket.OPEN){
            client.send("Stop Mission Exec");
          }
        }) 
      }
    })
    
    ws.on("close", () => {
      console.log("Client has disconnected!");
    });
});

 
//emits when socket is ready and listening for datagram msgs
server.on('listening',function(){
  var address = server.address();
  var port = address.port;
  var family = address.family;
  var ipaddr = address.address;
  console.log('Server is listening at port ' + port);
  console.log('Server ip :' + ipaddr);
  console.log('Server is IP4/IP6 : ' + family);
});

//emits after the socket is closed using socket.close();
server.on('close',function(){
  console.log('Socket is closed !');
});

server.bind(4208);
