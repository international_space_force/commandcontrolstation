import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Navbar from './components/NavigationBars/Navbar';
// React Router Import
import { Switch, Route } from 'react-router-dom';
// Import Pages
import Home from './components/pages/Home';
import Routes from './components/pages/Routes';
import PointsOfInterest from './components/pages/PointsOfInterest';
import ManualOverride from './components/pages/ManualOverride';
import NotFoundPage from './components/pages/NotFoundPage';

function App() {
  return (
    <div className="App">
      <Navbar />
      <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/routes" component={Routes} />
          <Route path="/points-of-interest" component={PointsOfInterest} />
          <Route path="/manual-override" component={ManualOverride} />
          <Route component={NotFoundPage} />
      </Switch>
    </div>
  );
}

export default App;
