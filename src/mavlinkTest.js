var mavlink = require('mavlink');
var myMAV = new mavlink(1,1);

myMAV.on("ready", function() {
    //parse incoming serial data
    serialport.on('data', function(data) {
        myMAV.parse(data);
    });
    
    //listen for messages
    myMAV.on("message", function(message) {
        console.log(message);
    });
});

myMAV.createMessage("RAW_PRESSURE",
        {
        'satellites_visible':		5,
        'satellite_prn':			[1, 2, 3, 4, 5],
        'satellite_used':			[2, 3, 4, 5, 6],
        'satellite_elevation':		[3, 4, 5, 6, 7],
        'satellite_azimuth':		[4, 5, 6, 7, 8],
        'satellite_snr':			[5, 6, 7, 8, 9]
        },
        function(message) {
            serialport.write(message.buffer);
        });